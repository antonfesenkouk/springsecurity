package net.example.springsecuritydemo.model;

public enum Status {
    ACTIVE, BANNED
}
